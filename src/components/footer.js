import React from "react"

const FooterLink = ({ children, ...props }) => (
  <a
    href="https://www.gatsbyjs.org"
    target="_blank"
    rel="noreferrer noopener"
    style={{ fontWeight: `700`, color: `black`, textDecoration: `none` }}
    {...props}
  >
    {children}
  </a>
)

const Footer = () => (
  <footer
    style={{
      paddingTop: `2rem`,
      paddingBottom: `2rem`,
      fontFamily: `Montserrat`,
      fontSize: `12px`,
      fontWeight: `500`,
      textAlign: `center`,
    }}
  >
    Made by Maksim Grudin. © Copiright 2020 <br />
    Built with <FooterLink href="https://www.gatsbyjs.org">Gatsby</FooterLink>
    .<br />
    Hosted on <FooterLink href="https://www.gitlab.com">GitLab</FooterLink>.
    Source available on GitLab.
  </footer>
)

export default Footer
