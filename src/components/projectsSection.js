import React from "react"

const ProjectsSection = ({ projects }) => (
  <section>
    <h2
      id="portfolio"
      style={{
        marginBottom: `2rem`,
        marginLeft: "2rem",
        fontFamily: "Six Caps",
        fontSize: "62px",
        fontWeight: `400`,
        letterSpacing: `10px`,
      }}
    >
      My works
    </h2>
    <div
      style={{
        marginLeft: "-3rem",
        paddingRight: "8rem",
        display: "flex",
        justifyContent: "space-between",
        flexWrap: "wrap",
      }}
    >
      {/* {projects.map(project => (
        <div
          style={{
            width: `100%`,
            maxWidth: `600px`,
            minWidth: `310px`,
            height: `400px`,
            backgroundColor: `rebeccapurple`,
            marginBottom: `2rem`,
          }}
        >
          {project}
        </div>
      ))} */}
    </div>
  </section>
)

export default ProjectsSection
