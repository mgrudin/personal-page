import React from "react"
import Image from "./image"

const IntroSection = () => (
  <section>
    <h1
      style={{
        marginBottom: `2rem`,
        marginLeft: "-3rem",
        fontFamily: "cardo",
        fontSize: "72px",
        fontWeight: `400`,
      }}
    >
      Maksim Grudin
    </h1>

    <p
      style={{
        fontFamily: `Carrois Gothic SC`,
        fontSize: `22px`,
        marginLeft: `2rem`,
      }}
    >
      Hello, I'm Front-end Developer from Mariupol', Ukraine.
    </p>
    <div
      style={{
        maxWidth: `500px`,
        marginBottom: `5rem`,
        marginLeft: `3rem`,
      }}
    >
      <Image />
    </div>
  </section>
)

export default IntroSection
