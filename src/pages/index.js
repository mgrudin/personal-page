import React from "react"
// import { Link } from "gatsby"

import Layout from "../components/layout"
import IntroSection from "../components/introSection"
import ProjectsSection from "../components/projectsSection"
import SEO from "../components/seo"

const IndexPage = props => (
  <Layout>
    <SEO title="Home" />
    <IntroSection />
    <ProjectsSection />
  </Layout>
)

export default IndexPage
