import { Link } from "gatsby"
import React from "react"

const Header = () => (
  <header>
    <div
      style={{
        display: "flex",
        justifyContent: "flex-end",
        padding: `2rem 10rem`,
      }}
    >
      <Link
        to="/"
        style={{
          marginRight: `1.5rem`,
          fontFamily: `Montserrat`,
          fontSize: `18px`,
          fontWeight: `700`,
          color: `#d3d3d3`,
          textDecoration: `none`,
        }}
      >
        home
      </Link>
      <Link
        to="#portfolio"
        style={{
          marginRight: `1.5rem`,
          fontFamily: `Montserrat`,
          fontSize: `18px`,
          fontWeight: `700`,
          color: `#d3d3d3`,
          textDecoration: `none`,
        }}
      >
        portfolio
      </Link>
      <Link
        to="#contacts"
        style={{
          fontFamily: `Montserrat`,
          fontSize: `18px`,
          fontWeight: `700`,
          color: `#d3d3d3`,
          textDecoration: `none`,
        }}
      >
        contacts
      </Link>
    </div>
  </header>
)

export default Header
