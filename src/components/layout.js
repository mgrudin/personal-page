import React from "react"
import PropTypes from "prop-types"

import Header from "./header"
import Footer from "./footer"
import "./layout.css"

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <main
        style={{
          marginLeft: "15rem",
          paddingTop: "2rem",
          paddingBottom: "3rem",
          backgroundColor: "#d6f9d4",
        }}
      >
        {children}
      </main>
      <Footer></Footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
